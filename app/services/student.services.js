const { Student } = require('../model');
const getList = async () => {
  const studentList1 = await Student.findAll();
  if (studentList1) return studentList1;
  else return false;
};
const getDetail = async (id) => {
  const student = await Student.findOne({
    where: {
      id,
    },
  });
  if (student) return student;
  else return false;
};
const create = async (student) => {
  const newStudent = await Student.create(student);

  return newStudent;
};
const update = async (id, student) => {
  const studentUpdate = await getDetail(id);
  if (studentUpdate) {
    studentUpdate.fullName = student.fullName;
    studentUpdate.age = student.age;
    studentUpdate.numberClass = student.numberClass;
    const studentUpdated = await studentUpdate.save();
    return studentUpdated;
  }
};
const deleteByid = async (id) => {
  const studentDelete = await getDetail(id);
  if (studentDelete) {
    await Student.destroy({
      where: {
        id,
      },
    });
    return studentDelete;
  } else {
    return false;
  }
};
module.exports = {
  getList,
  getDetail,
  create,
  update,
  deleteByid,
};
