const express = require('express');
const studentRouter = express.Router();
const {
  getStudentList,
  setStudentDetailById,
  createStudent,
  updateStudent,
  deleteStudent,
} = require('../controllers/student.controllers');
const { logFeature } = require('../middlewares/logger/log-feature');
const {
  checkEmpty,
  checkNumberClass,
} = require('../middlewares/validation/student.validation');

// lấy danh sách học sinh <=> (url: http://localhost:3000/students)
studentRouter.get('/', logFeature, getStudentList);
// lấy thông tin chi tiết học sinh (url: http://localhost:3000/students/:id)
studentRouter.get('/:id', setStudentDetailById);
// thêm học sinh
studentRouter.post('/', checkEmpty, checkNumberClass, createStudent);
// cập nhập học sinh
studentRouter.put('/:id', checkEmpty, checkNumberClass, updateStudent);
// xóa học sinh
studentRouter.delete('/:id', deleteStudent);
module.exports = studentRouter;
