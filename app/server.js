const express = require('express');
const router = require('./routers/root.router');
const app = express();
const port = 3000;
app.use(express.json()); // chuyển tất cả các request,respon về json để  thao tác
app.use(router);
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

// setup sequelize
const { sequelize } = require('./model');
sequelize.sync({ alter: true });
const checkConnect = async () => {
  try {
    await sequelize.authenticate();
    console.log('kết nối thành công');
  } catch (error) {
    console.log(' kết nối thất bại');
    console.log('error: ', error);
  }
};
checkConnect();
