const {
  getList,
  getDetail,
  create,
  update,
  deleteByid,
} = require('../services/student.services');
const getStudentList = async (req, res) => {
  const studentList = await getList();
  if (studentList) {
    res.status(200).send(studentList);
  } else {
    res.status(404).send('not found');
  }
};
const setStudentDetailById = async (req, res) => {
  const { id } = req.params;
  const student = await getDetail(id);
  if (student) {
    res.status(200).send(student);
  } else {
    res.status(404).send('Not found');
  }
};
const createStudent = async (req, res) => {
  let student = req.body;
  const newStudent = await create(student);
  res.status(201).send(newStudent);
};
const updateStudent = async (req, res) => {
  const { id } = req.params;
  const student = req.body;
  const studentUpdate = await update(id, student);
  if (studentUpdate) {
    res.status(200).send(studentUpdate);
  } else {
    res.status(404).send('Not found');
  }
};
const deleteStudent = async (req, res) => {
  const { id } = req.params;
  const student = await deleteByid(id);
  if (student) {
    res.status(200).send(student);
  } else {
    res.status(404).send('Not found');
  }
};
module.exports = {
  getStudentList,
  setStudentDetailById,
  createStudent,
  updateStudent,
  deleteStudent,
};
