const { DataTypes } = require('sequelize');

const createStudentModel = (sequelize) => {
  return sequelize.define(
    'Student',
    {
      fullName: {
        type: DataTypes.STRING, //VARCHAR(255)
        allowNull: false,
      },
      age: {
        type: DataTypes.INTEGER, //INT
        allowNull: false,
      },
      numberClass: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      tableName: 'students', //
      timestamps: true, // tắt createdAt updateAt
    },
  );
};
module.exports = {
  createStudentModel,
};
