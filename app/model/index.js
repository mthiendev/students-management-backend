const { DB, HOST, USER, PASSWORD, dialect } = require('../configs/db.configs');
const { Sequelize } = require('sequelize');
const { createStudentModel } = require('./student.model');
const sequelize = new Sequelize(DB, USER, PASSWORD, {
  host: HOST,
  dialect,
});
const Student = createStudentModel(sequelize);
module.exports = {
  sequelize,
  Student,
};
