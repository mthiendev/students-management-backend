const checkEmpty = (req, res, next) => {
  const { fullName, age, numberClass } = req.body;
  if (fullName && age && numberClass) {
    next();
  } else {
    res.status(500).send('Không đươc đề trống fullName,age,numberClass');
  }
};
const checkNumberClass = (req, res, next) => {
  const { numberClass } = req.body;
  if (numberClass >= 1 && numberClass <= 12) {
    next();
  } else {
    res.status(500).send('Vui lòng nhập lớp học từ 1 - 12');
  }
};
module.exports = {
  checkEmpty,
  checkNumberClass,
};
